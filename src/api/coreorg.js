import httpRequest from '@/utils/httpRequest'

export function getcoreorgList(param) {
  console.log(param)
  return httpRequest({
    url: httpRequest.adornUrl('/auth/coreorg/list/tree'),
    method: 'get',
    params: httpRequest.adornParams(param)
  })
}

/**
 * post 请求使用 data: httpRequest.adornData(param) 传值
 */
export function adduser(param) {
  return httpRequest({
    url: httpRequest.adornUrl('/sys/user/delete'),
    method: 'post',
    data: httpRequest.adornData(param)
  })
}

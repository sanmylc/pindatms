import http from '@/utils/httpRequest'

/*     司机作业单     */
//  查列表
export function getDriverJobList(params) {
    return http({
      url: http.adornUrl(`/work/driverjob/list`),
      method: 'get',
      params: params
    })
}



/*        取件、派件任务信息          */
//    取件
export function getQTaskPickupdispatchList(params) {
    return http({
        url: http.adornUrl('/work/taskpickupdispatch/qjlist'),
        method: 'get',
        params: http.adornParams(params)
    })
}
//    派件
export function getPTaskPickupdispatchList(params) {
  return http({
    url: http.adornUrl('/work/taskpickupdispatch/pjlist'),
    method: 'get',
    params: http.adornParams(params)
  })
}

export function getCTaskPickupdispatchList() {
  return http({
    url: http.adornUrl('/work/taskpickupdispatch/cjlist'),
    method: 'get'
  })
}
export function querycourier() {
  return http({
    url: http.adornUrl('/work/taskpickupdispatch/querycourier'),
    method: 'get'
  })
}
export function makecourier(params) {
  return http({
    url: http.adornUrl('/work/taskpickupdispatch/makecourier'),
    method: 'get',
    params: http.adornParams(params)
  })
}
export function updatestausbyid(params) {
  return http({
    url: http.adornUrl('/work/taskpickupdispatch/updatestatus/'+params),
    method: 'get'
  })
}



/*         运输任务表            */
export function getTaskTransportList(params) {
    return http({
        url: http.adornUrl('/work/tasktransport/list'),
        method: 'get',
        params: http.adornParams(params)
    })
}
//  根据id查
export function getTaskTransportInfo(id) {
    return http({
        url:  http.adornUrl('/work/tasktransport/info/'+id),
        method: 'get'
    })
}


/*         运单表         */
export function getTransportOrderList(params) {
  return http({
    url: http.adornUrl('/work/transportorder/list'),
    method: 'get',
    params: http.adornParams(params)
  })
}
//  根据id查
export function getTransportOrderInfo(params) {
    return http({
        url: http.adornUrl('/work/transportorder/info/'+params),
        method: 'get'
    })
}



/*          运单与运输任务关联表            */
export function getTransportOrderTaskList(params) {
    return http({
        url: '/work/transportordertask/list',
        method: 'get',
        params: params
    })
}
//  根据id查
export function getTransportOrderTaskInfo(params) {
    return http({
        url: '/work/transportordertask/info/'+params,
        method: 'get'
    })
}
//  添加
export function saveTransportOrderTask(params) {
    return http({
        url: '/work/transportordertask/save',
        method: 'post',
        params: params
    })
}
//  修改
export function updateTransportOrderTask(params) {
    return http({
        url: '/work/transportordertask/update',
        method: 'put',
        params: params
    })
}
//  删除
export function delTransportOrderTask(params) {
    return http({
        url: '/work/transportordertask/delete/'+params,
        method: 'delete'
    })
}



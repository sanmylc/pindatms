import httpRequest from '@/utils/httpRequest'

export function psottypeList(param) {
  return httpRequest({
    url: httpRequest.adornUrl('/base/goodstype/querygoodstypefy'),
    method: 'post',
    params: httpRequest.adornParams(param)
  })
}
  export function deltype(params) {
    return http({
      url: '/base/goodstype/delectgoodtypebyid/{id}'+params,
      method: 'delete'
    })
  }
  export function addtypeList(param) {
    return httpRequest({
      url: httpRequest.adornUrl('/base/goodstype/saveGoodsType'),
      method: 'post',
      data: httpRequest.adornData(param)
    })
  }
export function cheList(param) {
  return httpRequest({
    url: httpRequest.adornUrl('/base/trucktype/findtrucktypeAll'),
    method: 'post',
    params: httpRequest.adornParams(param)
  })
}
export function updateList(param) {
  return httpRequest({
    url: httpRequest.adornUrl('/base/trucktype//updategoodstypeById'
    ),
    method: 'put',
    params: httpRequest.adornParams(param)
  })
}

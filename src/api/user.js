import httpRequest from '@/utils/httpRequest'

/**
 * get 请求使用 params: httpRequest.adornParams(param) 传值
 */
export function getuserList(param) {
    return httpRequest({
        url: httpRequest.adornUrl('/sys/user/list'),
        method: 'get',
        params: httpRequest.adornParams(param)
    })
}

/**
 * post 请求使用 data: httpRequest.adornData(param) 传值
 */
export function adduser(param) {
    return httpRequest({
        url: httpRequest.adornUrl('/sys/user/delete'),
        method: 'post',
        data: httpRequest.adornData(param)
    })
}


/**
 * post 请求使用 data: httpRequest.adornData(param) 传值
 */
export function selectsheng(param) {
  return httpRequest({
    url: httpRequest.adornUrl('/auth/ssq/querybypid/'+param),
    method: 'get'
  })
}



export function getMenuId(param) {
    return httpRequest({
        url: httpRequest.adornUrl(`/sys/menu/info/${param}`),
        method: 'get',
        data: httpRequest.adornParams()
    })
}

export function getalldriver(param) {
  return httpRequest({
    url: httpRequest.adornUrl('/auth/authuser/getdriversum'),
    method: 'get',
  })
}

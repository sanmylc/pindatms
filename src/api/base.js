import http from '@/utils/httpRequest'

/**
 * get 请求使用 params: httpRequest.adornParams(param) 传值
 */
export function getaddress(pid) {
    return http({
        url: http.adornUrl('/auth/ssq/getaddress/'+pid),
        method: 'get'
    })
}
/* 分页模糊查询所有车队 */
export function getfleetinfo(params) {
  return http({
    url: http.adornUrl('/base/fleet/queryfleettypefy'),
    method: 'get',
    params: http.adornParams(params)
  })
}
/* 添加车队 */
export function addfleet(param) {
  return http({
    url: http.adornUrl('/base/fleet/savefleet'),
    method: 'post',
    params: http.adornParams(param)
  })
}
/* 修改车队信息 */
export function updatefleet(param) {
  return http({
    url: http.adornUrl('/base/fleet/updatefleetbyid'),
    method: 'put',
    params: http.adornParams(param)
  })
}
/* 根据id查车队信息 */
export function getfleetbyid(id) {
  return http({
    url: http.adornUrl('/base/fleet/finefleetById/'+id),
    method: 'get'
  })
}
/*  根据i删除车队   */
export function delfleetbyid(id) {
  return http({
    url: http.adornUrl('/base/fleet/delectfleetbyid/'+id),
    method: 'delete'
  })
}
/*    获取车队列表 */
export function gettrucklist(id) {
  return http({
    url: http.adornUrl('/base/fleet/selecttruckbyfleetid/'+id),
    method: 'get'
  })
}
/*   获取司机列表   */
export function getdriverlist(id) {
  return http({
    url: http.adornUrl('/base/fleet/selectTruckDriverbyfleetid/'+id),
    method: 'get'
  })
}

/*   获取司机列表   */
export function getcororglist() {
  return http({
    url: http.adornUrl('/auth/coreorg/list/tree'),
    method: 'get'
  })
}


/* 分页模糊查询所有车辆 */
export function gettruckinfo(params) {
  return http({
    url: http.adornUrl('/base/truck/queryTruckfy'),
    method: 'get',
    params: http.adornParams(params)
  })
}
/* 添加车辆 */
export function addtruck(param) {
  return http({
    url: http.adornUrl('/base/truck/saveTruckfy'),
    method: 'post',
    params: http.adornParams(param)
  })
}
/*   获取车辆类型列表   */
export function gettrucktypelist() {
  return http({
    url: http.adornUrl('/base/trucktype/findtrucktypeAll'),
    method: 'get'
  })
}
/*   获取车队列表   */
export function getfleetlist() {
  return http({
    url: http.adornUrl('/base/fleet/findfleetAll'),
    method: 'get'
  })
}

/*  根据i删除车队   */
export function deltruckbyid(id) {
  return http({
    url: http.adornUrl('/base/truck/deleteTruckEntitybyid/'+id),
    method: 'delete'
  })
}
/* 根据id查车辆详情 */
export function gettruckdetails(id) {
  return http({
    url: http.adornUrl('/base/truck/gettruckinfobyid/'+id),
    method: 'get'
  })
}


/* 分页模糊查询所有线路 */
export function getlineinfo(params) {
  return http({
    url: http.adornUrl('/base/transportline/querytransportLinefy'),
    method: 'get',
    params: http.adornParams(params)
  })
}
/*  根据id删除线路   */
export function dellinebyid(id) {
  return http({
    url: http.adornUrl('/base/transportline/delectTransportLinebyid/'+id),
    method: 'delete'
  })
}
/* 根据id查线路详情 */
export function getlinedetails(id) {
  return http({
    url: http.adornUrl('/base/transportline/fineTransportLineById/'+id),
    method: 'get'
  })
}
/* 添加线路 */
export function addline(param) {
  return http({
    url: http.adornUrl('/base/transportline/saveTransportLine'),
    method: 'get',
    params: http.adornParams(param)
  })
}
/*   获取线路类型   */
export function getlinetypelist() {
  return http({
    url: http.adornUrl('/base/transportlinetype/findTransportLineTypeAll'),
    method: 'get'
  })
}

/* 修改线路 */
export function updatelinebyid(param) {
  return http({
    url: http.adornUrl('/base/transportline/updateTransportLinebyid'),
    method: 'put',
    params: http.adornParams(param)
  })
}


/* 分页模糊查询所有线路 */
export function getlinetypeinfo(params) {
  return http({
    url: http.adornUrl('/base/transportlinetype/findTransportLineTypeByPage'),
    method: 'get',
    params: http.adornParams(params)
  })
}
/*  根据id删除线路   */
export function dellinetypebyid(id) {
  return http({
    url: http.adornUrl('/base/transportlinetype/delectlxlxbyid/'+id),
    method: 'delete'
  })
}
/* 根据id查线路详情 */
export function getlinetypedetails(id) {
  return http({
    url: http.adornUrl('/base/transportlinetype/fineTransportLineTypeById/'+id),
    method: 'get'
  })
}
/* 添加线路 */
export function addlinetype(param) {
  return http({
    url: http.adornUrl('/base/transportlinetype/saveTransportLineType'),
    method: 'post',
    params: http.adornParams(param)
  })
}
/* 修改线路 */
export function updatelinetypebyid(param) {
  return http({
    url: http.adornUrl('/base/transportlinetype/updateTransportLineTypebyid'),
    method: 'put',
    params: http.adornParams(param)
  })
}



/* 添加车次 */
export function addtripsinfo(param) {
  return http({
    url: http.adornUrl('/base/transporttrips/saveTransportTrips'),
    method: 'post',
    params: http.adornParams(param)
  })
}
/* 根据id查车次信息 */
export function gettripsinfo(id) {
  return http({
    url: http.adornUrl('/base/transporttrips/fineTransportTripsById/'+id),
    method: 'get'
  })
}

/* 修改车次 */
export function updatetripsbyid(param) {
  return http({
    url: http.adornUrl('/base/transporttrips/updateTransportTripsbyid'),
    method: 'put',
    params: http.adornParams(param)
  })
}
/*  根据id删除车次   */
export function deletetripsbyid(id) {
  return http({
    url: http.adornUrl('/base/transporttrips/delectTransportTripsbyid/'+id),
    method: 'delete'
  })
}
/* 查寻车辆信息 */
export function gettruckinfolist() {
  return http({
    url: http.adornUrl('/base/truck/gettrucklist'),
    method: 'get'
  })
}

/* 安排车辆 */
export function maketrucks(param) {
  return http({
    url: http.adornUrl('/base/transporttripstruckdriver/transporttripstruckdriversaveTruck'),
    method: 'post',
    params: http.adornParams(param)
  })
}
/* 查询车辆司机信息   */
export function gettruckdriverinfo(id) {
  return http({
    url: http.adornUrl('/base/transporttripstruckdriver/getinfo/'+id),
    method: 'get'
  })
}
/* 查寻司机信息 */
export function getdriverinfos() {
  return http({
    url: http.adornUrl('/auth/authuser/getdriverinfo'),
    method: 'get'
  })
}
/* 安排车辆 */
export function maketruckdrivers(param) {
  return http({
    url: http.adornUrl('/base/transporttripstruckdriver/transporttripstruckdriversaveTruckDriver'),
    method: 'post',
    params: http.adornParams(param)
  })
}


/* 查询待发司机数量 */
export function getalldfdriver() {
  return http({
    url: http.adornUrl('/base/truckdriver/getdfdriver'),
    method: 'get',
  })
}
/* 查新在途司机 */
export function getallztdriver() {
  return http({
    url: http.adornUrl('/base/truckdriver/getztdriver'),
    method: 'get',
  })
}

/* 查询车辆状态详情 */
export function gettruckinfos() {
  return http({
    url: http.adornUrl('/work/tasktransport/gettruckstatus'),
    method: 'get',
  })
}


/* 查询运输详情 */
export function gettransportinfos() {
  return http({
    url: http.adornUrl('/work/tasktransport/gettruckstatus'),
    method: 'get',
  })
}

/* 查询所有省份 */
export function getprovinceinfo() {
  return http({
    url: http.adornUrl('/oms/order/getorderprovince'),
    method: 'get',
  })
}









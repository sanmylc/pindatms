## 安装

npm install
或
npm install --force

## 运行

npm start

npm install webpack-dev-server --save-dev

'webpack-dev-server' 不是内部或外部命令，也不是可运行的程序执行
npm install webpack-dev-server --save-dev

```js

/**
 * git 请求使用 params: httpRequest.adornParams(param) 传值
 */
export function getuserList(param) {
    return httpRequest({
        url: httpRequest.adornUrl('/sys/user/list'),
        method: 'get',
        params: httpRequest.adornParams(param)
    })
}
/**
 * git 路径传值使用方法
 */
export function getMenuId(param) {
    return httpRequest({
        url: httpRequest.adornUrl(`/sys/menu/info/${param}`),
        method: 'get',
        data: httpRequest.adornParams()
    })
}

/**
 * post 请求使用 data: httpRequest.adornData(param) 传值
 */
export function adduser(param) {
    return httpRequest({
        url: httpRequest.adornUrl('/sys/user/delete'),
        method: 'post',
        data: httpRequest.adornData(param)
    })
}

```
http://pinda.itheima.net/

